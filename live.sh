#!/bin/bash

rm -r ./.live/
mkdir ./.live/
cp -f src/index.html ./.live/
cp -rf assets ./.live/
elm-live --dir "./.live/" --start-page "index.html" -- src/Main.elm --output "./.live/app.js" --debug
