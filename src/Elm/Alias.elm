module Elm.Alias exposing (Alias, Aliases, Definition(..), aliasesView, view)

import Element exposing (Element, text)
import Layout exposing (indent, indentings)


type alias Alias =
    { name : String
    , definition : Definition
    }


type alias Aliases =
    List Alias


type Definition
    = RecordDefinition (List ( String, String ))


view : Alias -> Element msg
view a =
    Element.column []
        [ text <| typeAliasNameString a
        , definitionView a.definition
        ]


typeAliasNameString : Alias -> String
typeAliasNameString t =
    "type alias " ++ t.name ++ " ="


definitionView : Definition -> Element msg
definitionView definition =
    Element.el [ indentings ] <|
        case definition of
            RecordDefinition [ ( k, v ) ] ->
                Element.el
                    []
                    (text <| "{ " ++ k ++ " : " ++ v ++ " }")

            _ ->
                text "NOT IMPLEMENTED"


aliasesView : Aliases -> Element msg
aliasesView aliases =
    case aliases of
        [] ->
            Element.none

        _ ->
            Element.column [ Element.spacing indent ] <|
                List.map view aliases
