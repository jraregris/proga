module Elm.Expose exposing (Expose(..), exposeString)


type Expose
    = All
    | Some (List String)


exposeString : Expose -> String
exposeString e =
    case e of
        All ->
            ".."

        Some list ->
            String.join ", " list
