module Elm.Expression exposing (Expression(..), Record, expressionView, unit)

import Element exposing (Element, text)
import Layout exposing (charWidth, indentings)


type Expression
    = LiteralString String
    | LiteralRecord Record
    | RecordUpdate Expression Updatings
    | LiteralInt Int
    | Variable String
    | FunctionCall String Expressions
    | Todo
    | CaseExp Case
    | InfixExp Expression String Expression
    | LiteralList Expressions
    | Unit


type alias Record =
    List RecordEntry


type alias RecordEntry =
    ( String, Expression )


type alias Updatings =
    List ( String, Expression )


type alias Case =
    { exp : Expression
    , cases : List ( CasePattern, Expression )
    }


type alias CasePattern =
    String


type alias Expressions =
    List Expression


unit : Expression
unit =
    Unit


recordLine : ( String, Expression ) -> Int -> Element msg
recordLine ( name, exp ) index =
    let
        leftPad =
            case index of
                0 ->
                    "{ "

                _ ->
                    ", "
    in
    Element.row []
        [ text leftPad
        , Element.row
            []
            [ text name, text " = ", expressionView exp ]
        ]


recordView : Record -> Element msg
recordView record =
    case record of
        [ ( name, LiteralInt i ) ] ->
            Element.el [] <|
                text ("{ " ++ name ++ " = " ++ String.fromInt i ++ " }")

        _ ->
            Element.column []
                [ Element.column [ Element.spacingXY 0 6 ]
                    (List.indexedMap
                        (\index ( n, e ) -> recordLine ( n, e ) index)
                        record
                    )
                , Element.text "}"
                ]


expressionView : Expression -> Element msg
expressionView e =
    case e of
        LiteralString s ->
            text ("\"" ++ s ++ "\"")

        LiteralRecord r ->
            recordView r

        FunctionCall fName [] ->
            Element.el [] <| text fName

        FunctionCall fName params ->
            Element.row [ Element.spacing 12 ] <|
                [ Element.el [ Element.alignTop ] (text fName)
                , Element.column [] <|
                    List.map expressionView params
                ]

        Variable name ->
            text name

        LiteralInt i ->
            text (i |> String.fromInt)

        Todo ->
            text "Debug.Todo"

        Unit ->
            text "()"

        CaseExp c ->
            Element.column []
                [ Element.row []
                    [ text "case "
                    , expressionView c.exp
                    , text " of"
                    ]
                , Element.column [ indentings ]
                    (c.cases
                        |> List.map
                            (\( k, v ) ->
                                Element.column []
                                    [ text (k ++ " ->")
                                    , Element.el [ indentings ] (expressionView v)
                                    ]
                            )
                    )
                ]

        RecordUpdate v u ->
            Element.row []
                [ text "{ "
                , expressionView v
                , text " | "
                , Element.row []
                    (u
                        |> List.map
                            (\( name, ex ) ->
                                Element.row []
                                    [ text <| name ++ " = "
                                    , expressionView ex
                                    , text <| " }"
                                    ]
                            )
                    )
                ]

        InfixExp left infix right ->
            Element.row [ Element.spacing charWidth ]
                [ expressionView left
                , text infix
                , expressionView right
                ]

        LiteralList l ->
            case l of
                [] ->
                    Element.el [] <| text "[]"

                [ exxxx ] ->
                    Element.row [] [ text "[ ", expressionView exxxx, text " ]" ]

                firstExp :: restExps ->
                    Element.column []
                        [ Element.row []
                            [ Element.el [ Element.alignTop ] (text "[ ")
                            , expressionView firstExp
                            ]
                        , Element.column []
                            (List.map
                                (\exxx ->
                                    Element.row []
                                        [ Element.el [ Element.alignTop ] (text ", ")
                                        , expressionView exxx
                                        ]
                                )
                                restExps
                            )
                        , text "]"
                        ]
