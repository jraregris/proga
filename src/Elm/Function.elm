module Elm.Function exposing (Function, Functions, functionGenerator, functionView, functionsView)

import Element exposing (Element, text)
import Elm.Expression as Expression exposing (Expression, expressionView)
import Layout exposing (charWidth, indent, indentings)
import Random
import Random.Char
import Random.String


type alias Function =
    { name : String
    , sig : String
    , parameters : List String
    , exp : Expression
    }


type alias Functions =
    List Function


functionView : Function -> Element msg
functionView f =
    Element.column []
        [ text <|
            (f.name ++ " : " ++ f.sig)
        , functionViewSecondRow f
        , Element.el [ indentings ] <| expressionView f.exp
        ]


functionViewSecondRow : Function -> Element msg
functionViewSecondRow f =
    case f.parameters of
        [] ->
            text <| f.name ++ " ="

        _ ->
            Element.row [ Element.spacing charWidth ]
                [ text <| f.name
                , Element.row [ Element.spacing charWidth ]
                    (List.map text f.parameters)
                , text <| "="
                ]


functionsView : Functions -> Element msg
functionsView functions =
    case functions of
        [] ->
            Element.none

        _ ->
            Element.column [ Element.spacing indent ] <|
                List.map functionView functions


functionGenerator : Random.Generator Function
functionGenerator =
    functionNameGenerator |> Random.map (\name -> { name = name, sig = "()", parameters = [], exp = Expression.unit })


functionNameGenerator : Random.Generator String
functionNameGenerator =
    Random.String.rangeLengthString 1 20 Random.Char.english
        |> Random.map String.toLower
