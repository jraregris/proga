module Elm.Import exposing (new)

import Elm.Import.Type exposing (Import)


new : String -> List String -> Import
new name expose =
    { name = name, expose = expose }
