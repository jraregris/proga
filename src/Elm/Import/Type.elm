module Elm.Import.Type exposing (Import, Imports)


type alias Import =
    { name : String
    , expose : List String
    }


type alias Imports =
    List Import
