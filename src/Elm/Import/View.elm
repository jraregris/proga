module Elm.Import.View exposing (importsView, view)

import Element exposing (Element, text)
import Elm.Import.Type exposing (Import, Imports)


view : Import -> a -> Element msg
view i _ =
    Element.el [] <| text <| importString i


importString : Import -> String
importString i =
    case i.expose of
        [] ->
            "import " ++ i.name

        exposings ->
            "import " ++ i.name ++ " exposing (" ++ String.join ", " exposings ++ ")"


importsView : Imports -> a -> Element msg
importsView imports _ =
    case imports of
        [] ->
            Element.none

        _ ->
            let
                vf =
                    \i -> view i ()
            in
            Element.column [] <| List.map vf imports
