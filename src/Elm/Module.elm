module Elm.Module exposing (addFunction, moduleGenerator, moduleNameGenerator, module_, new, with_imports)

import Elm.Alias exposing (Aliases)
import Elm.Expose exposing (Expose)
import Elm.Function exposing (Function, Functions)
import Elm.Import.Type exposing (Imports)
import Elm.Module.Type exposing (Module)
import Elm.Type exposing (Types)
import Random
import Random.Char
import Random.String


new : String -> Imports -> Expose -> Types -> Aliases -> Functions -> Module
new name imports expose types aliases funcs =
    { name = name
    , imports = imports
    , expose = expose
    , types = types
    , aliases = aliases
    , funcs = funcs
    }


module_ : String -> Module
module_ name =
    new name [] Elm.Expose.All [] [] []


with_imports : Imports -> Module -> Module
with_imports imports m =
    { m | imports = imports }


moduleNameGenerator : Random.Generator String
moduleNameGenerator =
    Random.String.rangeLengthString 1 20 Random.Char.english
        |> Random.map String.uncons
        |> Random.map (Maybe.withDefault ( 'X', "" ))
        |> Random.map (\( c, s ) -> ( c |> Char.toUpper, s |> String.toLower ))
        |> Random.map (\( c, s ) -> String.cons c s)


moduleGenerator : Random.Generator Module
moduleGenerator =
    moduleNameGenerator |> Random.map module_


addFunction : Function -> Module -> Module
addFunction func mod =
    { mod | funcs = mod.funcs ++ [ func ] }
