module Elm.Module.Type exposing (Module, Modules)

import Elm.Alias exposing (Aliases)
import Elm.Expose exposing (Expose)
import Elm.Function exposing (Functions)
import Elm.Import.Type exposing (Imports)
import Elm.Type exposing (Types)


type alias Module =
    { name : String
    , imports : Imports
    , expose : Expose
    , types : Types
    , aliases : Aliases
    , funcs : Functions
    }


type alias Modules =
    List Module
