module Elm.Module.View exposing (moduleView, modulesView)

import Element exposing (Attribute, Element, text)
import Element.Events as Events
import Elm.Alias exposing (aliasesView)
import Elm.Expose exposing (exposeString)
import Elm.Function exposing (functionsView)
import Elm.Import.View exposing (importsView)
import Elm.Module.Type exposing (Module, Modules)
import Elm.Type exposing (typesView)
import Html exposing (s)
import Layout exposing (pad)
import Msg exposing (Msg)


modulesView : Modules -> Maybe String -> Element Msg
modulesView modules selected =
    let
        lf =
            \m -> moduleView m selected
    in
    Element.wrappedRow [ Element.spacing pad ] (List.map lf modules)


moduleView : Module -> Maybe String -> Element Msg
moduleView m selected =
    moduleBlox m selected |> bloxToElement (selected == Nothing)


moduleBlox : Module -> Maybe String -> Blox
moduleBlox m selected =
    let
        s =
            case selected of
                Nothing ->
                    NotSelected

                Just selectedName ->
                    if selectedName == m.name then
                        Selected

                    else
                        NotSelected

        mark =
            case selected of
                Nothing ->
                    []

                Just selectedName ->
                    case s of
                        Selected ->
                            []

                        NotSelected ->
                            Layout.marked

        selectStuff =
            case selected of
                Nothing ->
                    [ Element.pointer
                    , Events.onClick (Msg.SelectModule m.name)
                    ]

                Just selectedName ->
                    case s of
                        Selected ->
                            []

                        NotSelected ->
                            [ Element.pointer
                            , Events.onClick (Msg.SelectModule m.name)
                            ]

        attribs =
            [ Element.padding pad
            , Element.spacing pad
            ]
                ++ mark
                ++ Layout.box
                ++ selectStuff
    in
    blox s
        attribs
        (\a ->
            Element.column
                a
            <|
                [ moduleNameAndExposingView m
                , importsView m.imports ()
                , aliasesView m.aliases
                , typesView m.types
                , functionsView m.funcs
                ]
        )


type alias Blox =
    { selected : Selected, attributes : List (Attribute Msg), element : List (Attribute Msg) -> Element Msg }


blox : Selected -> List (Attribute Msg) -> (List (Attribute Msg) -> Element Msg) -> Blox
blox selected attributes element =
    { selected = selected, element = element, attributes = attributes }


type Selected
    = Selected
    | NotSelected


bloxToElement : Bool -> Blox -> Element Msg
bloxToElement anyoneSelected b =
    let
        mark =
            if anyoneSelected then
                case b.selected of
                    Selected ->
                        []

                    NotSelected ->
                        Layout.marked

            else
                []
    in
    b.element b.attributes


moduleNameAndExposingView : Module -> Element Msg
moduleNameAndExposingView m =
    Element.row []
        [ text <| "module "
        , Element.el [] (text <| m.name)
        , text <| " exposing (" ++ exposeString m.expose ++ ")"
        ]
