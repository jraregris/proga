module Elm.Program exposing (Program, addModule, addModuleWithName, new, removeModuleWithName, view)

import Element
import Elm.Module
import Elm.Module.Type exposing (Module, Modules)
import Elm.Module.View exposing (modulesView)
import Msg exposing (Msg)


type alias Program =
    Modules


new : Program
new =
    []


addModule : Module -> Program -> Program
addModule mod p =
    if p |> hasModuleWithName mod.name then
        p

    else
        p ++ [ mod ]


hasModuleWithName : String -> Program -> Bool
hasModuleWithName name program =
    program |> List.any (\modul -> modul.name == name)


addModuleWithName : String -> Program -> Program
addModuleWithName name p =
    p ++ [ Elm.Module.module_ name ]


removeModuleWithName : String -> Program -> Program
removeModuleWithName name p =
    p
        |> List.filterMap
            (\m ->
                if m.name == name then
                    Nothing

                else
                    Just m
            )


view : Program -> Maybe String -> Element.Element Msg
view program selected =
    Element.column [] [ modulesView program selected ]
