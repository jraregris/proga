module Elm.Type exposing (Type, Types, typesView, view)

import Element exposing (Element, text)
import Layout exposing (indent, indentings)


type alias Type =
    { name : String
    , constructors : List String
    }


type alias Types =
    List Type


view : Type -> Element msg
view t =
    Element.column []
        [ text <| typeNameString t
        , Element.column [ indentings ]
            ((text <| firstTypeConstructorString t)
                :: theRestOfTheTypeConstructors t
            )
        ]


typeNameString : Type -> String
typeNameString t =
    "type " ++ t.name


firstTypeConstructorString : Type -> String
firstTypeConstructorString t =
    case t.constructors of
        one :: _ ->
            "= " ++ one

        [] ->
            ""


theRestOfTheTypeConstructors : Type -> List (Element msg)
theRestOfTheTypeConstructors t =
    case t.constructors of
        _ :: consts ->
            consts
                |> List.map
                    (\c -> text <| "| " ++ c)

        _ ->
            []


typesView : Types -> Element msg
typesView types =
    case types of
        [] ->
            Element.none

        _ ->
            Element.column [ Element.spacing indent ] <|
                List.map view types
