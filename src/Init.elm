module Init exposing (init)

import Elm.Alias exposing (Alias, Definition(..))
import Elm.Expose exposing (Expose(..))
import Elm.Expression exposing (Expression(..))
import Elm.Function exposing (Function)
import Elm.Import as Import
import Elm.Module as Module
import Elm.Module.Type exposing (Module)
import Elm.Type exposing (Type)
import Model exposing (Model)


init : Model
init =
    { program = [], selected = Nothing }


helloCounter : Module
helloCounter =
    Module.new "Main"
        [ Import.new "Browser" []
        , Import.new "Html" [ "Html", "button", "div", "text" ]
        , Import.new "Html.Events" [ "onClick" ]
        ]
        All
        [ Type "Msg" [ "Increment", "Decrement" ] ]
        [ Alias "Model" (RecordDefinition [ ( "count", "Int" ) ]) ]
        [ Function "initalModel" "Model" [] (LiteralRecord [ ( "count", LiteralInt 0 ) ])
        , Function "update"
            "Msg -> Model -> Model"
            [ "msg", "model" ]
            (CaseExp
                { exp = Variable "msg"
                , cases =
                    [ ( "Increment"
                      , RecordUpdate (Variable "model")
                            [ ( "count", InfixExp (Variable "model.count") "+" (LiteralInt 1) ) ]
                      )
                    , ( "Decrement"
                      , RecordUpdate (Variable "model")
                            [ ( "count", InfixExp (Variable "model.count") "-" (LiteralInt 1) ) ]
                      )
                    ]
                }
            )
        , Function "view"
            "Model -> Html Msg"
            [ "model" ]
            (FunctionCall
                "div"
                [ LiteralList []
                , LiteralList
                    [ FunctionCall "button"
                        [ LiteralList [ FunctionCall "onClick" [ Variable "Increment" ] ]
                        , LiteralList [ FunctionCall "text" [ LiteralString "+1" ] ]
                        ]
                    , FunctionCall "div"
                        [ LiteralList []
                        , LiteralList
                            [ InfixExp (FunctionCall "text" [])
                                "<|"
                                (FunctionCall "String.fromInt" [ FunctionCall "model.count" [] ])
                            ]
                        ]
                    , FunctionCall "button"
                        [ LiteralList [ FunctionCall "onClick" [ Variable "Decrement" ] ]
                        , LiteralList [ FunctionCall "text" [ LiteralString "-1" ] ]
                        ]
                    ]
                ]
            )
        , Function "main"
            "Program () Model Msg"
            []
            (FunctionCall "Browser.sandbox"
                [ LiteralRecord
                    [ ( "init", Variable "initalModel" )
                    , ( "view", Variable "view" )
                    , ( "update", Variable "update" )
                    ]
                ]
            )
        ]
