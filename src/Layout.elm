module Layout exposing (box, bright, charHeight, charWidth, dark, global, heading, indent, indentings, inverted, marked, marker, marker2, pad)

import Element
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font


pad : Int
pad =
    charWidth * 2


charHeight : Int
charHeight =
    20


indentings : Element.Attribute msg
indentings =
    Element.paddingEach
        { top = 0
        , right = 0
        , bottom = 0
        , left = indent
        }


indent : Int
indent =
    charWidth * 2


charWidth : Int
charWidth =
    12


global : List (Element.Attribute msg)
global =
    [ Font.family [ Font.typeface "Iosevka Web", Font.monospace ]
    , Font.color dark
    , Font.size charHeight
    , Background.color bright
    , Background.tiled "assets/2x2.svg"
    , Element.padding 10
    ]


heading : List (Element.Attr () msg)
heading =
    box


inverted : List (Element.Attr () msg)
inverted =
    [ Font.color bright, Background.color dark ]


bright : Element.Color
bright =
    Element.rgb255 0xF0 0xF6 0xF0


dark : Element.Color
dark =
    Element.rgb255 0x22 0x23 0x23


box : List (Element.Attr () msg)
box =
    [ Background.color bright
    , Border.solid
    , Border.widthEach
        { left = 1
        , top = 1
        , right = 2
        , bottom = 2
        }
    ]


marked : List (Element.Attr () msg)
marked =
    [ Background.color (Element.rgb 1 1 1)
    , Background.tiled "assets/2x2-3.svg"
    ]


marker : Element.Element msg
marker =
    Element.el
        [ Element.width (Element.px charWidth)
        , Background.tiled "assets/2x2.svg"
        ]
        (Element.text
            " "
        )


marker2 : Element.Element msg
marker2 =
    Element.el
        [ Element.width (Element.px charWidth)
        , Background.tiled "assets/2x2-2.svg"
        ]
        (Element.text
            " "
        )
