module Main exposing (main, subscriptions)

import Browser
import Init
import Model exposing (Model)
import Msg exposing (Msg)
import Update exposing (update)
import View exposing (view)


main : Program () Model Msg
main =
    Browser.document
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }


init : () -> ( Model, Cmd Msg )
init _ =
    ( Init.init, Cmd.none )


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none
