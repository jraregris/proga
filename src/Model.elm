module Model exposing (Model)

import Elm.Program exposing (Program)


type alias Model =
    { program : Program
    , selected : Maybe String
    }
