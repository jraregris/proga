module Msg exposing (Msg(..))

import Elm.Function exposing (Function)
import Elm.Module.Type exposing (Module)


type Msg
    = AddModuleButtonClicked
    | AddModule Module
    | AddFunction String Function
    | AddFunctionButtonClicked
    | RenameModule String String
    | SelectModule String
    | RenameModuleButtonClicked
    | DeselectModule
    | DeselectModuleButtonClicked
    | RemoveModule String
    | RemoveModuleButtonClicked
