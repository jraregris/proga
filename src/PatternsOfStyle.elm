module PatternsOfStyle exposing (main)

import Browser
import Html
import Msg exposing (Msg)


main : Program () () Msg
main =
    Browser.document
        { init = \_ -> ( (), Cmd.none )
        , view = \_ -> { title = "Pattern of Style", body = [ Html.div [] [] ] }
        , update = \_ model -> ( model, Cmd.none )
        , subscriptions = always Sub.none
        }
