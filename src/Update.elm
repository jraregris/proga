module Update exposing (update)

import Elm.Function
import Elm.Module exposing (moduleNameGenerator)
import Elm.Program as Program
import Model exposing (Model)
import Msg exposing (Msg(..))
import Random


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        AddFunction moduleName function ->
            let
                prog =
                    model.program
                        |> List.map
                            (\modul ->
                                if modul.name == moduleName then
                                    { modul | funcs = modul.funcs ++ [ function ] }

                                else
                                    modul
                            )
            in
            ( { model | program = prog }, Cmd.none )

        AddFunctionButtonClicked ->
            case model.selected of
                Nothing ->
                    ( model, Cmd.none )

                Just selectedModuleName ->
                    ( model, Random.generate (AddFunction selectedModuleName) Elm.Function.functionGenerator )

        AddModule module_ ->
            ( { model | program = model.program |> Program.addModule module_ }, Cmd.none )

        AddModuleButtonClicked ->
            ( model, Random.generate AddModule Elm.Module.moduleGenerator )

        SelectModule name ->
            ( { model | selected = Just name }, Cmd.none )

        RenameModuleButtonClicked ->
            case model.selected of
                Nothing ->
                    ( model, Cmd.none )

                Just selectedModuleName ->
                    ( model, Random.generate (RenameModule selectedModuleName) Elm.Module.moduleNameGenerator )

        RenameModule oldname newname ->
            ( { model
                | program =
                    model.program
                        |> specialMap
                            (\m -> m.name == oldname)
                            (\m -> { m | name = newname })
                , selected = Just newname
              }
            , Cmd.none
            )

        DeselectModuleButtonClicked ->
            -- This might be an antipattern
            update DeselectModule model

        DeselectModule ->
            ( { model | selected = Nothing }, Cmd.none )

        RemoveModule moduleName ->
            ( { model | program = model.program |> Program.removeModuleWithName moduleName, selected = Nothing }, Cmd.none )

        RemoveModuleButtonClicked ->
            case model.selected of
                Nothing ->
                    ( model, Cmd.none )

                Just selectedModuleName ->
                    update (RemoveModule selectedModuleName) model


specialMap : (a -> Bool) -> (a -> a) -> List a -> List a
specialMap filter mapper list =
    list
        |> List.map
            (\element ->
                if filter element then
                    mapper element

                else
                    element
            )
