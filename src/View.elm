module View exposing (view)

import Browser
import Element exposing (Element)
import Element.Input
import Elm.Program as Program
import Layout
import Model exposing (Model)
import Msg exposing (Msg)


view : Model -> Browser.Document Msg
view model =
    { title = "Proga"
    , body =
        [ Element.layout
            Layout.global
            (Element.column
                [ Element.spacing Layout.charHeight ]
                [ headingRow
                , moduleBar model.selected
                , Program.view model.program model.selected
                ]
            )
        ]
    }


moduleBar : Maybe String -> Element.Element Msg
moduleBar selected =
    let
        buttons =
            if selected == Nothing then
                [ addModuleBar ]

            else
                [ renameModuleButton, deselectModuleButton, removeModuleButton, addFunctionButton ]
    in
    Element.wrappedRow [ Element.spacing Layout.charWidth ] buttons


deselectModuleButton : Element.Element Msg
deselectModuleButton =
    Element.Input.button (Layout.box ++ [ Element.focused [] ])
        { onPress = Just Msg.DeselectModuleButtonClicked
        , label = Element.text " deselect module "
        }


removeModuleButton : Element.Element Msg
removeModuleButton =
    Element.Input.button (Layout.box ++ [ Element.focused [] ])
        { onPress = Just Msg.RemoveModuleButtonClicked
        , label = Element.text " remove module "
        }


addModuleBar : Element.Element Msg
addModuleBar =
    Element.Input.button (Layout.box ++ [ Element.focused [] ])
        { onPress = Just Msg.AddModuleButtonClicked
        , label = Element.text " add module "
        }


renameModuleButton : Element.Element Msg
renameModuleButton =
    Element.Input.button (Layout.box ++ [ Element.focused [] ])
        { onPress = Just Msg.RenameModuleButtonClicked
        , label = Element.text " rename module "
        }


addFunctionButton : Element.Element Msg
addFunctionButton =
    Element.Input.button (Layout.box ++ [ Element.focused [] ])
        { onPress = Just Msg.AddFunctionButtonClicked
        , label = Element.text " add function "
        }


headingRow : Element.Element msg
headingRow =
    Element.row ([] ++ Layout.heading)
        [ Layout.marker
        , Element.text " PROGA "
        ]
