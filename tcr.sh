#!/bin/bash

clear

echo "PROGA TCR"

# Check if code is good, revert and exit if bad
elm make --output=/dev/null src/Main.elm \
&& elm-test \
|| (git reset --hard && echo "REVERT!" && exit 1) || exit

git add -A
git diff --cached  --patience

tcr_msg=$(git log -1 --pretty=%B)

read -p "'$tcr_msg'? (Y/n)" -n 1 -r

# new line
echo ""

case $REPLY in
    n)
        read -p "msg: " tcr_msg
        ;;
esac

git commit -m "${tcr_msg}" || exit
git push &