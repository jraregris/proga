module ModuleTest exposing (suite)

import Elm.Expose
import Elm.Import as Import
import Elm.Module exposing (with_imports)
import Expect
import Test exposing (Test, describe, test)


suite : Test
suite =
    describe "The Module Module"
        [ describe "Builder"
            [ describe "initial"
                [ test "name is name" <|
                    \_ ->
                        Expect.equal module_.name "Module"
                , test "no imports" <|
                    \_ ->
                        Expect.equal module_.imports []
                , test "expose all" <|
                    \_ ->
                        Expect.equal module_.expose Elm.Expose.All
                , test "no types" <|
                    \_ ->
                        Expect.equal module_.types []
                , test "no aliases" <|
                    \_ ->
                        Expect.equal module_.aliases []
                , test "no functions" <|
                    \_ ->
                        Expect.equal module_.funcs []
                ]
            , test "with_imports" <|
                \_ ->
                    let
                        imports =
                            [ Import.new "Browser" []
                            , Import.new "Html" [ "Html", "button", "div", "text" ]
                            , Import.new "Html.Events" [ "onClick" ]
                            ]

                        mwi =
                            module_
                                |> with_imports imports
                    in
                    Expect.equal mwi.imports imports
            ]
        ]


module_ =
    Elm.Module.module_ "Module"
