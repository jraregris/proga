module ProgramFuzz exposing (program)

import Elm.Module
import Elm.Module.Type exposing (Module)
import Elm.Program exposing (Program)
import Fuzz exposing (Fuzzer)
import Random
import Random.Char
import Random.String
import Shrink


program : Fuzzer Program
program =
    Fuzz.list moduleFuzzer


moduleFuzzer : Fuzzer Module
moduleFuzzer =
    Fuzz.custom moduleGenerator Shrink.noShrink


moduleGenerator : Random.Generator Module
moduleGenerator =
    name |> Random.map (\n -> Elm.Module.module_ n)


name : Random.Generator String
name =
    Random.String.rangeLengthString 1 50 Random.Char.latin
