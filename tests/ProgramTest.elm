module ProgramTest exposing (suite)

import Elm.Module as Module
import Elm.Program as Program
import Expect
import ProgramFuzz
import Test


suite : Test.Test
suite =
    Test.describe "Program"
        [ Test.test "new Program has no module" <|
            \_ ->
                Program.new
                    |> List.length
                    |> Expect.equal 0
        , Test.fuzz ProgramFuzz.program "Adding a module increases the module count by 1" <|
            \program ->
                program
                    |> Program.addModule (Module.module_ "honk")
                    |> List.length
                    |> Expect.equal (1 + (program |> List.length))
        , Test.fuzz ProgramFuzz.program "Adding a module that has the same name as an existing module does not change the module count" <|
            \program ->
                program
                    |> Program.addModule (Module.module_ "honk")
                    |> Program.addModule (Module.module_ "honk")
                    |> List.length
                    |> Expect.equal (1 + (program |> List.length))
        ]
