module ViewTest exposing (..)

import Elm.Module as Module
import Elm.Module.View as View
import Expect exposing (within)
import Test exposing (Test, test)


suite : Test
suite =
    test "selected module should not be clickable" <|
        \_ ->
            let
                mod =
                    Module.module_ "test"

                selected =
                    Nothing

                element =
                    View.moduleView mod selected
            in
            Expect.equal 1 1
